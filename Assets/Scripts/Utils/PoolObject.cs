﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolObject
{

}

public class PoolObject<T> : MonoBehaviour where T : MonoBehaviour
{
    public GameObject _object;
    public int _initItems;
    public Dictionary<GameObject, bool> _pool;

    void Awake()
    {
        for (int i = 0; i < _initItems; i++)
            CreateObject();
    }

    void CreateObject()
    {
        GameObject _newObject = Instantiate(_object, transform);

    }

    public void GetObject()
    {

    }
}

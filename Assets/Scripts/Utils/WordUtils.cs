﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character
{
    public int ratio;
    public int score;
    public string character;

    public static Character NONE = new Character(" ", 300, 0);

    public Character(string character,int ratio,int score)
    {
        this.character = character;
        this.ratio = ratio;
        this.score = score;
    }

}

public static class WordUtils  {
    static string ratioSuffix = "RATIO";
    static string scoreSuffix = "SCORE";
    static string CHARACTER = "CHARACTERS";

    static List<Character> listCharacters = new List<Character>
    {
        {new Character("E",24,1)},
        {new Character("T",15,1)},
        {new Character("A",16,1)},
        {new Character("O",15,1)},
        {new Character("I",13,1)},
        {new Character("N",13,1)},
        {new Character("S",10,1)},
        {new Character("R",13,1)},
        {new Character("H",5,4)},
        {new Character("D",8,2)},
        {new Character("L",7,1)},
        {new Character("U",7,1)},
        {new Character("C",6,3)},
        {new Character("M",6,3)},
        {new Character("F",4,4)},
        {new Character("Y",4,4)},
        {new Character("W",4,4)},
        {new Character("G",5,2)},
        {new Character("P",4,3)},
        {new Character("B",4,3)},
        {new Character("V",3,4)},
        {new Character("K",2,5)},
        {new Character("X",2,8)},
        {new Character("Q",2,10)},
        {new Character("J",2,8)},
        {new Character("Z",2,10)}
    };

    static List<Character> wordPool;


    public static void InitWordPool()
    {
        wordPool = new List<Character>();
        foreach (Character c in listCharacters)
        {
            for (int i = 0; i < c.ratio; i++)
            {
                wordPool.Add(c);
            }
        }
    }

    public static Character GetRandomCharacter()
    {
        if(wordPool==null)
        {
            InitWordPool();
        }

        if (wordPool.Count == 0)
        {
            InitWordPool();
        }

        int index = Random.Range(0, wordPool.Count);

        Character character = wordPool[index];

        wordPool.RemoveAt(index);

        return character;
    }

    public static void LoadWordConfig()
    {
        if (PlayerPrefs.GetString(CHARACTER, "") == "")
        {

        }
        else
        {
            string characters = PlayerPrefs.GetString(CHARACTER);
            string[] words = characters.Split(' ');
            foreach(string word in words)
            {
                int ratio = PlayerPrefs.GetInt(word + ratioSuffix,0);
                int score = PlayerPrefs.GetInt(word + scoreSuffix, 0);
                Character c = new Character(word, ratio, score);
                listCharacters.Add(c);
            }
        }

    }

    public static void SaveWordConfig()
    {
        string characters = "";
        foreach(Character c in listCharacters)
        {
            characters += c.character;
            characters += " ";
            PlayerPrefs.SetInt(c.character+ratioSuffix, c.ratio);
            PlayerPrefs.SetInt(c.character + scoreSuffix, c.score);
        }
        if (characters.EndsWith(" "))
            characters.Remove(characters.Length - 1);
        PlayerPrefs.SetString(CHARACTER, characters);
    }


}

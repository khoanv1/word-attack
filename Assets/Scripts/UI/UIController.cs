﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class UIController : Singleton<UIController> {


    public UIPlayer uiPlayer;
    public UIPlayer uiOpponent;

    public TextMeshProUGUI currentWordText;
    public TextMeshProUGUI messageText;


    public GameObject restart;


    public Text text1;

    public Text text2;

    public Text text3;


    public GameObject playerTurn;
    public GameObject opponentTurn;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        text1.text = Input.touchCount.ToString();
        text2.text = Input.GetMouseButton(0).ToString();
       //text3.text = Input.GetMouseButton(0).ToString();
    }


    public void UpdateCurrentWord(string s = "")
    {
        currentWordText.SetText(s);
    }

    public void ShowNotice(string s)
    {
        messageText.SetText(s);
        Debug.Log(s);
    }


    public void Restart()
    {
        BoardGameController.Instance.Restart();
        //restart.SetActive(false);
    }

    public void ShowEndGame()
    {
        restart.SetActive(true);
    }

    public void SetPlayerTurn(PlayerType playerType)
    {
        playerTurn.SetActive(playerType == PlayerType.PLAYER);
        opponentTurn.SetActive(playerType == PlayerType.OPPONENT);
    }


    public void OnbtnRemoveBlank()
    {
        BoardGameController.Instance.RemoveBlank();
    }


    public void OnbtnBooster2()
    {

    }

    public void OnbtnBooster3()
    {

    }

}

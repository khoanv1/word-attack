﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIPlayer : MonoBehaviour {

    public TextMeshProUGUI actionText;
    public TextMeshProUGUI timeText;
    public TextMeshProUGUI healText;
    public TextMeshProUGUI numberCharacterText;

    public void UpdateAction(int action)
    {
        actionText.SetText(action.ToString());
    }

    public void UpdateHeal(int heal)
    {
        healText.SetText(heal.ToString());
    }

    public void UpdateTime(float time)
    {
        timeText.SetText(((int)time).ToString());
    }

    public void UpdateNumberCharactersDelete(int n)
    {
        numberCharacterText.SetText(n.ToString());
    }

}

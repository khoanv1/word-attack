﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class EffectController : Singleton<EffectController> {
    public GameObject effectAttack;
    public GameObject effectAttack2;

    public GameObject PlayEffectAttack(Vector3 position)
    {
        //Debug.Log(position);
        GameObject eff = GameObject.Instantiate(effectAttack,transform);
        eff.transform.position = position;
        eff.transform.localScale = Vector3.zero;
        eff.transform.DOScale(1f, 1f);
        Destroy(eff, 1.5f);
        return eff;
    }


    public GameObject PlayEffectAttack2(Vector3 position)
    {
        Debug.Log(position);
        GameObject eff = GameObject.Instantiate(effectAttack2, transform);
        eff.transform.position = position;

        return eff;
    }
}

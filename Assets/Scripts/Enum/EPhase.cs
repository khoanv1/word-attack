﻿public enum EPhase
{
    NONE=0,
    SWAPTURN,
    ACTION,
    ATTACK,
    ENDTURN
}
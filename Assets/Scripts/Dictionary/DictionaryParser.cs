﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Text;
using System;

public static class DictionaryParser {

    static string path = "Assets/Resources/corncob_caps.txt";
    static string dictionaryPath = "Assets/Resources/dictionary.txt";
    static string[] words;
    static List<string> valibWords = new List<string>();
	// Use this for initialization
	public static void ParseDictionary() {
        string s = System.IO.File.ReadAllText(path);
        s = s.Replace(((char)13).ToString(), "");
        words = s.Split('\n');

        foreach (string word in words)
        {
            if (word.Length>=2 && word.Length<=8 && word.All(char.IsLetter))
            {
                valibWords.Add(word);
                //Debug.Log(word);
                //Debug.Log(word.Length);
            }
            else
            {

            }
        }
        Debug.Log("number of word " + words.Length);
       
        //string newDictionary = "";
        //foreach (string word in words)
        //{
        //    newDictionary += word;
        //    newDictionary += "\n";
        //}

        System.IO.StreamWriter file = new System.IO.StreamWriter(dictionaryPath, true);
        foreach (string word in valibWords)
            file.WriteLine(word);
        Debug.Log("number of valib word " + valibWords.Count);
        file.WriteLine("end");
    }



    public static string StringToBinary(string data)
    {
        StringBuilder sb = new StringBuilder();

        foreach (char c in data.ToCharArray())
        {
            sb.Append(Convert.ToString(c, 2).PadLeft(8, '0'));
        }
        return sb.ToString();
    }
}

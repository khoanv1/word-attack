﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dictionary : Singleton<Dictionary> {

    static string path = "dictionary";

    string[] words = { };

    void Awake()
    {
        TextAsset asset = Resources.Load(path) as TextAsset;
      //  Debug.Log(asset.text);
        string s = asset.text;
        words = s.Split('\n');
    }



    public bool IsValidWord(string checkingWord)
    {
        foreach (string word in words)
        {
            if(word.ToLower().Equals(checkingWord.ToLower()))
            {
                return true;
            }
        }

        return false;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
public class Tools
{

    [MenuItem("Tools/Parse dictionary")]
    public static void ParseDictionary()
    {
        DictionaryParser.ParseDictionary();
    }

}
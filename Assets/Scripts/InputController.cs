﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SelectType
{
    None=0,
    Horizontal,
    Vertical
}

public enum InputType
{
    None=1,
    Drag,
    Tap

}



public class InputController : Singleton<InputController> {

    public LayerMask layer;

    public InputType inputType = InputType.None;
    public SelectType selectType = SelectType.None;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (BoardGameController.Instance.phase != EPhase.ACTION)
            return;
        if(inputType == InputType.Tap)
        {
            if (Input.GetMouseButtonUp(0))
            {
                var hit = Physics2D.Raycast(
                (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition),
                Vector2.zero, Mathf.Infinity, layer);
                if (hit.collider != null)
                {
                  //  Debug.Log("swap block");
                    Block block = hit.transform.GetComponent<Block>();
                    if (block != null )
                    {
                   //     Debug.Log("swap block");
                        BoardGameController.Instance.SwapBlock(block);

                    }
                }
                inputType = InputType.None;
            }
        }


        else if (Input.GetMouseButton(0)
#if !UNITY_EDITOR
            && Input.touchCount>0 
#endif
            )
        {
            UIController.Instance.text3.text = "select";
            var hit = Physics2D.Raycast(
                (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition),
                Vector2.zero, Mathf.Infinity, layer);
            if (hit.collider != null)
            {
                Block block = hit.transform.GetComponent<Block>();


                if (block != null )
                {
                    BoardGameController.Instance.SelectBlock(block);

                }
            }
        } else
        {
            UIController.Instance.text3.text = "unselect";
            BoardGameController.Instance.UnSelectBlock();
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourcesController  {

    static ResourcesController instance;
    public static ResourcesController Instance
    {
        get
        {
            if (instance == null)
            instance = new ResourcesController();
            return instance;
        }
    }


    public Dictionary<string, Sprite> dictionary = new Dictionary<string, Sprite>();


    public Sprite LoadResource(string path)
    {
        if(!dictionary.ContainsKey(path))
        {
            Sprite s = Resources.Load<Sprite>(path);
            dictionary.Add(path, s);
        }
        return dictionary[path];
    }

}

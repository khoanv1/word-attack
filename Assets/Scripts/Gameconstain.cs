﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Gameconstain  {

    //board
    public static int GAME_ROW=8;
    public static int GAME_COL=8;
    public static int MAX_WORD_LENGTH = 8;


    //game info
    public static int MAX_HEALTH=100;
    public static float TIME_PER_TURN = 30f;
    public static int ACTION_PER_TURN = 5;
    public static int COUNTDOWN_ATTACK = 2;


    //remove blank chacter
    public static int NUMBER_BLANK_CHARACTER_REMOVE=5;
    public static int NUMBER_CHARACTER = 25;


    //sprite name
    public static string BORDER_SPRITE = "Sprites/border";


    //animation effect
    public static float TIME_ANIMATE_BLOCK = 0.5f;
    public static Color attackColor = Color.red;
    public static Color defenseColor = Color.green;
    public static Color tapColor = Color.yellow;
}

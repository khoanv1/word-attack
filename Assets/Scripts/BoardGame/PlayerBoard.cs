﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System.Linq;
using TMPro;


public enum EWORD_TYPE
{
    NONE,
    SWAP,
    ATTACK,
    DEFENSE
}


[System.Serializable]
public class WordAction
{
    public EWORD_TYPE wordType;
    public List<Block> blocks=new List<Block>();
    public int strength;
    public int countDown;

    public GameObject ui;
    public TextMeshPro text;

    public WordBorderEffect border;

    public WordAction(EWORD_TYPE actionType,List<Block> blocks)
    {

        countDown = Gameconstain.COUNTDOWN_ATTACK;
        this.wordType = actionType;
        this.blocks = blocks;
        foreach (Block block in blocks)
        {
            strength += block.score;
        }

        if (blocks.Count > 0)
        {
            if(actionType == EWORD_TYPE.ATTACK)
                text = blocks[0].totalAttackText;
            else
                text = blocks[0].totalDefenseText;
        }
        text.gameObject.SetActive(true);

        List<Block> tempBlocks = blocks.OrderBy(x => x.GetCoordinate().x + x.GetCoordinate().y).ToList();
        int lastIndex = tempBlocks.Count - 1;
        Vector3 firstBlockPosition = tempBlocks[0].transform.position;
        Vector3 lastBlockPosition = tempBlocks[lastIndex].transform.position;

        Vector3 center = (firstBlockPosition + lastBlockPosition) / 2;
        Vector3 size = new Vector3(Mathf.Abs(firstBlockPosition.x - lastBlockPosition.x) + blocks[0].blockSprite.bounds.size.x,
                    Mathf.Abs(firstBlockPosition.y - lastBlockPosition.y) + blocks[0].blockSprite.bounds.size.y);
        GameObject wordBorder = new GameObject();
        border = wordBorder.AddComponent<WordBorderEffect>();
        border.DrawBorder(center,size, actionType);
        UpdateStrength();
    }

    public void UpdateStrength()
    {
        text.text = strength.ToString();
    }

    public override string ToString()
    {
        string s = "";
        foreach(Block block in blocks)
        {
            s += block.GetCharacter();
        }
        return s;
    }



}

[System.Serializable]
public class PlayerBoard
{
    //_blocks[row,col]
    public Block[,] _blocks;

    public Vector3 originePosition = Vector3.zero;
    public float _blockWidth = 0.55f;
    public float _blockHight = 0.55f;
    public bool upsideDown;
    private int health = Gameconstain.MAX_HEALTH;
    private int numberCharacterDelete = 0;
    public List<WordAction> wordActions= new List<WordAction>();

    private int actionCount = Gameconstain.ACTION_PER_TURN;

    public UIPlayer ui;

    public void InitBoardGame()
    {
        int row = Gameconstain.GAME_ROW;
        int col = Gameconstain.GAME_COL;
        _blocks = new Block[row, col];
        for (int i = 0; i < row; i++)
        {
            for (int j = 0; j < col; j++)
            {
                GameObject newObj = BoardGameController.Instance.GetBlockFromPool();
                Block newBlock = newObj.GetComponent<Block>();
                newObj.SetActive(true);
                UpdateBlock(newBlock, i, j);
                newBlock.SetCharacter(Character.NONE);
            }
        }

        int characterCount = Gameconstain.NUMBER_CHARACTER;
        characterCount = Mathf.Min(characterCount, row * col);


        while(characterCount>0)
        {
            int i = Random.Range(0, row);
            int j = Random.Range(0, col);
            if(_blocks[i,j].GetCharacter()==Character.NONE.character)
            {
                _blocks[i, j].SetCharacter(WordUtils.GetRandomCharacter());
                characterCount--;
            }
        }



        UpdatePosition();
        health = Gameconstain.MAX_HEALTH;
        ui.UpdateHeal(health);

        actionCount = Gameconstain.ACTION_PER_TURN;
        ui.UpdateAction(actionCount);
    }


    public void ClearBoard()
    {
        int row = Gameconstain.GAME_ROW;
        int col = Gameconstain.GAME_COL;
       
        for (int i = 0; i < row; i++)
        {
            for (int j = 0; j < col; j++)
            {
                if(_blocks[i,j]!=null)
                {
                    Object.Destroy(_blocks[i, j].gameObject);
                    _blocks[i, j] = null;
                }
            }
        }

        while(wordActions.Count>0)
        {
            Object.Destroy(wordActions[0].border.gameObject);
            wordActions.RemoveAt(0);
        }

    }

    public void UpdateBlock(Block block, int x, int y)
    {
        _blocks[x, y] = block;
        block.SetCoordinate(new Coordinate(x, y));
    }


    public void UpdatePosition()
    {
        int row = Gameconstain.GAME_ROW;
        int col = Gameconstain.GAME_COL;
        for (int i = 0; i < row; i++)
        {
            for (int j = 0; j < col; j++)
            {
                if (_blocks[i, j] != null)
                    _blocks[i, j].transform.position = GetworldPosition(i, j);
            }
        }
    }

    public Vector3 GetworldPosition(int row, int col)
    {

        return originePosition
                + _blockWidth * Vector3.right * (row - ((Gameconstain.GAME_ROW - 1f) / 2))
                + _blockHight * Vector3.up * (upsideDown ? -col : col);
    }

    public Vector3 GetworldPosition(Coordinate coor)
    {
        return GetworldPosition(coor.x, coor.y);
    }

    public bool ConstainBlock(Block block)
    {
        for (int i = 0; i < Gameconstain.GAME_ROW; i++)
        {
            for (int j = 0; j < Gameconstain.GAME_COL; j++)
            {
                if (block == _blocks[i, j])
                    return true;
            }
        }
        return false;
    }

    //remove block
    //public void RemoveBlock(List<Block> blocks)
    //{
    //    for (int i = 0; i < Gameconstain.GAME_ROW; i++)
    //    {
    //        for (int j = 0; j < Gameconstain.GAME_COL; j++)
    //        {
    //            if (blocks.Contains(this._blocks[i, j]))
    //            {
    //                BoardGameController.Instance.ReturnToPool(_blocks[i, j].gameObject);
    //                _blocks[i, j] = null;
    //            }
    //        }
    //    }
    //    RearrangeBoard();
    //}

    public void RemoveBlankCharacter()
    {
        if (numberCharacterDelete == 0)
            return;
        int blank_counter=0;
        for (int i = 0; i < Gameconstain.GAME_ROW; i++)
        {
            for (int j = 0; j < Gameconstain.GAME_COL; j++)
            {
                if (_blocks[i, j].IsBlank())
                    blank_counter++;
            }
        }


        blank_counter = Mathf.Min(blank_counter,numberCharacterDelete);

        while(blank_counter>0)
        {
            int i = Random.Range(0, Gameconstain.GAME_ROW);
            int j = Random.Range(0, Gameconstain.GAME_COL);
            if(_blocks[i, j] != null && _blocks[i,j].IsBlank())
            {
                blank_counter--;
                BoardGameController.Instance.ReturnToPool(_blocks[i, j].gameObject);
                _blocks[i, j] = null;
            }

        }
        RearrangeBoard();
        CountAction();
        numberCharacterDelete = 0;
        ui.UpdateNumberCharactersDelete(numberCharacterDelete);
    }

    public void RemoveWords(List<WordAction> words)
    {
        while (words.Count > 0)
        {
            while(words[0].blocks.Count>0)
            {
                bool isDestroy = true;
                //
                if (wordActions.Any(x => x!=words[0] && x.blocks.Contains(words[0].blocks[0])))
                    isDestroy = false;
                if (isDestroy == true)
                {
                    Coordinate coor = words[0].blocks[0].GetCoordinate();
                    BoardGameController.Instance.ReturnToPool(_blocks[coor.x, coor.y].gameObject);
                    _blocks[coor.x, coor.y] = null;
                }
                words[0].blocks.RemoveAt(0);
            }
            words[0].text.gameObject.SetActive(false);
            Object.Destroy(words[0].border.gameObject);
            //
            wordActions.Remove(words[0]);
            words.RemoveAt(0);
        }
        //RearrangeBoard();
    }

    int LowestAvaiable(int col)
    {
        for (int i = 0; i < Gameconstain.GAME_ROW; i++)
        {
            if (_blocks[col, i] == null)
                return i;
        }
        return Gameconstain.GAME_ROW;
    }

    //rearrange block
    public void RearrangeBoard()
    {

        int newChar = 0;
        for (int i = 0; i < Gameconstain.GAME_ROW; i++)
        {
            for (int j = 0; j < Gameconstain.GAME_COL; j++)
            {
                if (_blocks[i, j] == null)
                {
                    GameObject newObj = BoardGameController.Instance.GetBlockFromPool();
                    Block newBlock = newObj.GetComponent<Block>();
                    newObj.SetActive(true);
                    UpdateBlock(newBlock, i, j);
                    newBlock.SetCharacter(Character.NONE);
                    _blocks[i, j].transform.position = GetworldPosition(i, j);

                    float targetScale = _blocks[i, j].transform.localScale.x;
                    _blocks[i, j].transform.localScale = Vector3.zero;
                    _blocks[i, j].transform.DOScale(Vector3.one * targetScale, 0.5f);
                    newChar++;
                }
            }
        }

        for(int i=0;i<newChar;i++)
        {
            int x = Random.Range(0, Gameconstain.GAME_ROW);
            int y = Random.Range(0, Gameconstain.GAME_COL);
            while(!_blocks[x, y].IsEmpty())
            {
                 x = Random.Range(0, Gameconstain.GAME_ROW);
                 y = Random.Range(0, Gameconstain.GAME_COL);
            }
            _blocks[x, y].SetCharacter(WordUtils.GetRandomCharacter());

        }


    }
    //create new block


    //swap blocks
    public void SwapBlock(Block block1, Block block2)
    {
        if (!ConstainBlock(block1) || !ConstainBlock(block2))
            return;
        Coordinate coor1 = block1.GetCoordinate();
        Coordinate coor2 = block2.GetCoordinate();
        UpdateBlock(block1, coor2.x, coor2.y);
        UpdateBlock(block2, coor1.x, coor1.y);
        block1.transform.position = GetworldPosition(block1.GetCoordinate());
        block2.transform.position = GetworldPosition(block2.GetCoordinate());

        CountAction();
    }

    public void AddWord(EWORD_TYPE actionType, List<Block> blocks)
    {
        foreach(WordAction word in wordActions)
        {
            if (Enumerable.SequenceEqual(word.blocks.OrderBy(x => x.GetInstanceID()), blocks.OrderBy(x => x.GetInstanceID())))
                return;
        }

        foreach (WordAction word in wordActions)
        {
            if (blocks.All(x =>word.blocks.Contains(x)))
            {
                return;
            }
        }
        bool isParentWord = false;
        WordAction childWord = null;
        foreach (WordAction word in wordActions)
        {
            if (word.blocks.All(x => blocks.Contains(x)))
            {
                childWord = word;
                isParentWord = true;
                break;
            }
        }

        WordAction wordAction = new WordAction(actionType, blocks);
        foreach (Block block in blocks)
            block.isNotMove = true;
        wordActions.Add(wordAction);
        if(isParentWord)
        {
            Object.Destroy(childWord.border.gameObject);
            wordActions.Remove(childWord);
        }
        numberCharacterDelete += blocks.Count;
        CountAction();
        ui.UpdateNumberCharactersDelete(numberCharacterDelete);
    }

    public void WordAttack()
    {
        foreach(WordAction word in wordActions)
        {
            if(word.wordType == EWORD_TYPE.ATTACK)
            {
                word.countDown--;
                if(word.countDown<=0)
                {
                    Debug.Log(word.ToString()+" attack");
                    BoardGameController.Instance.Attack(word);
                }
            }
        }
    }

    public List<WordAction> GetWordDefenseAtCol(int col)
    {
        List<WordAction> wordDefense = new List<WordAction>();
        foreach (WordAction word in wordActions)
            if ( word.blocks.Any(x => x.GetCoordinate().x == col))
                wordDefense.Add(word);
        return wordDefense;
    }


    public void ResetActionAcount()
    {
        actionCount = Gameconstain.ACTION_PER_TURN;
        ui.UpdateAction(actionCount);
    }


    void CountAction()
    {
        actionCount--;
        if (actionCount <= 0)
            BoardGameController.Instance.SetPhase(EPhase.ATTACK);
        ui.UpdateAction(actionCount);
    }

    public void ReduceHeal(int value)
    {
        health -= value;
        ui.UpdateHeal(health);
    }

    public bool IsAlive()
    {
        return health > 0;
    }

}
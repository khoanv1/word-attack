﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WordBorderEffect : MonoBehaviour {

    float borderSize = 0.06f;
   //SpriteRenderer sprite;

    public void DrawBorder(Vector3 center,Vector2 size,EWORD_TYPE type)
    {
        SpriteRenderer spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        if (spriteRenderer == null)
        {
            spriteRenderer = gameObject.AddComponent<SpriteRenderer>();
            Sprite sprite = ResourcesController.Instance.LoadResource(Gameconstain.BORDER_SPRITE);
            spriteRenderer.sprite = sprite;
        }

        transform.position = center;
        spriteRenderer.drawMode = SpriteDrawMode.Sliced;
        spriteRenderer.size = size + Vector2.one * borderSize;
        spriteRenderer.sortingOrder = -1;

        switch (type)
        {
            case EWORD_TYPE.ATTACK:
                spriteRenderer.color = Gameconstain.attackColor;
                break;
            case EWORD_TYPE.DEFENSE:
                spriteRenderer.color = Gameconstain.defenseColor;
                break;
            case EWORD_TYPE.SWAP:
                spriteRenderer.color = Gameconstain.tapColor;
                break;
            default:
                break;
        }
        //s.size = size;
    }

    public void DrawBorder(Vector3 center, Vector2 size, SelectType type)
    {
        SpriteRenderer spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        spriteRenderer.enabled = true;
        transform.position = center;
        spriteRenderer.drawMode = SpriteDrawMode.Sliced;
        spriteRenderer.size = size + Vector2.one * borderSize;

        switch (type)
        {
            case SelectType.Vertical:
                spriteRenderer.color = Gameconstain.attackColor;
                break;
            case SelectType.Horizontal:
                spriteRenderer.color = Gameconstain.defenseColor;
                break;
            case SelectType.None:
                spriteRenderer.color = Gameconstain.tapColor;
                break;
            default:
                break;
        }
        //s.size = size;
    }

    public void ClearBorder()
    {
        SpriteRenderer spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        spriteRenderer.enabled = false;
    }
}

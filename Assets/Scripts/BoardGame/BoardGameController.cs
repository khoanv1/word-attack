﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System.Linq;



public class BoardGameController : Singleton<BoardGameController>
{
    [SerializeField]
    PlayerBoard playerBoard;
    [SerializeField]
    PlayerBoard opponentBoard;


    PlayerBoard myboard;
    PlayerBoard currentPlayerBoard;
    PlayerBoard waitingPlayerBoard;


    PlayerType currentPlayerTurn = PlayerType.NONE;
    public EPhase phase = EPhase.NONE;

    //prefab block
    public GameObject _block;


    public List<Block> selectedBlocls = new List<Block>();

    public UIPlayer ui;

    public WordBorderEffect borderCurrentWord;

    // Use this for initialization
    void Start()
    {
        DOTween.defaultEaseType = Ease.InSine;
        StartGame();
    }


    void StartGame()
    {
        WordUtils.InitWordPool();

        playerBoard.ui = UIController.Instance.uiPlayer;
        opponentBoard.ui = UIController.Instance.uiOpponent;
        playerBoard.InitBoardGame();
        opponentBoard.InitBoardGame();

        FirstTurn();
    }


    void FirstTurn()
    {
        currentPlayerTurn = PlayerType.PLAYER;
        currentPlayerBoard = playerBoard;
        waitingPlayerBoard = opponentBoard;
        currentPlayerBoard.ResetActionAcount();
        UIController.Instance.SetPlayerTurn(currentPlayerTurn);
        SetPhase(EPhase.SWAPTURN);
    }

    void SwitchTurn()
    {
        if (currentPlayerTurn == PlayerType.OPPONENT)
        {
            currentPlayerTurn = PlayerType.PLAYER;
            currentPlayerBoard = playerBoard;
            waitingPlayerBoard = opponentBoard;
        }
        else
        {
            currentPlayerTurn = PlayerType.OPPONENT;
            currentPlayerBoard = opponentBoard;
            waitingPlayerBoard = playerBoard;
        }
        UIController.Instance.SetPlayerTurn(currentPlayerTurn);
        currentPlayerBoard.ResetActionAcount();
        SetPhase(EPhase.SWAPTURN);
    }


    public void SetPhase(EPhase phase)
    {
        this.phase = phase;
        if (phase == EPhase.SWAPTURN)
            DOTween.Sequence().AppendInterval(1).AppendCallback(() => SetPhase(EPhase.ACTION));
        else if (phase == EPhase.ATTACK)
            WordAttack();
    }


    public void WordAttack()
    {
        //currentPlayerBoard.WordAttack();

        ////destroy blocks
        ////listDestroyWords
        //waitingPlayerBoard.RemoveWords(new List<WordAction>(listDestroyWords));
        //listDestroyWords.Clear();


        //currentPlayerBoard.RemoveWords(new List<WordAction>(listWordAttacked));
        //listWordAttacked.Clear();
        ////create new block
        ////

        //CheckEndGame();
        StartCoroutine(AttackAnimate());
    }

    IEnumerator AttackAnimate()
    {
        currentPlayerBoard.WordAttack();


        foreach(WordAction word in listWordAttacked)
        {
            GameObject obj = EffectController.Instance.PlayEffectAttack2(currentPlayerBoard.GetworldPosition(word.blocks[0].GetCoordinate()));
            obj.transform.DOMoveY(obj.transform.position.y + (currentPlayerTurn == PlayerType.PLAYER ? 10 : -10), 1f).SetDelay(0.5f);
            Destroy(obj, 1.5f);
        }

        yield return new WaitForSeconds(0.5f);
        currentPlayerBoard.RemoveWords(new List<WordAction>(listWordAttacked));
        listWordAttacked.Clear();

        yield return new WaitForSeconds(0.2f);
        currentPlayerBoard.RearrangeBoard();

        waitingPlayerBoard.RemoveWords(new List<WordAction>(listDestroyWords));
        listDestroyWords.Clear();

        yield return new WaitForSeconds(0.2f);
        waitingPlayerBoard.RearrangeBoard();
        //RearrangeBoard();

        yield return null;

        CheckEndGame();
    }


    void CheckEndGame()
    {
        if (currentPlayerBoard.IsAlive() && waitingPlayerBoard.IsAlive())
        {
            SwitchTurn();
        }
        else
        {
            UIController.Instance.ShowEndGame();
        }
    }

    public GameObject GetBlockFromPool()
    {
        return GameObject.Instantiate(_block, transform);
    }

    public void ReturnToPool(GameObject obj)
    {
        Destroy(obj);
    }


    SelectType selectType = SelectType.None;
    public void SelectBlock(Block block)
    {
    
        if (!currentPlayerBoard.ConstainBlock(block))
            return;
        if (selectedBlocls.Contains(block))
        {
            if(selectedBlocls[selectedBlocls.Count-1]==block)
            {
                return;
            }
            else
            {
                //remove tail
                int index = selectedBlocls.IndexOf(block);
                while (selectedBlocls.Count > index+1)
                {
                    selectedBlocls.RemoveAt(index + 1);

                }
                HightLightBorder();
            }
        }
        if (selectedBlocls.Count >= Gameconstain.MAX_WORD_LENGTH)
            return;
        int wordcount = selectedBlocls.Count;

        if (selectedBlocls.Count == 0)
        {
            selectedBlocls.Add(block);
            HightLightBorder();
            UIController.Instance.UpdateCurrentWord(GetCurrentWord());
            if (!block.IsSelectAble())
            {
                InputController.Instance.inputType = InputType.Tap;
            }
        }
        else if (selectedBlocls.Count == 1)
        {
            if (selectedBlocls[0].GetCoordinate().x == block.GetCoordinate().x
            && Mathf.Abs(selectedBlocls[wordcount - 1].GetCoordinate().y - block.GetCoordinate().y) == 1)
            {
                selectType = SelectType.Vertical;
                SelectCharacter(block);
            }
            else if (selectedBlocls[0].GetCoordinate().y == block.GetCoordinate().y
            && Mathf.Abs(selectedBlocls[wordcount - 1].GetCoordinate().x - block.GetCoordinate().x) == 1)
            {
                selectType = SelectType.Horizontal;
                SelectCharacter(block);
            }
            else
            {
                return;
            }
        }
        else
        {
            if (selectType == SelectType.Vertical)
            {
                if (selectedBlocls[0].GetCoordinate().x == block.GetCoordinate().x
                && Mathf.Abs(selectedBlocls[wordcount - 1].GetCoordinate().y - block.GetCoordinate().y) == 1)
                {
                    SelectCharacter(block);
                }
                else
                {
                    return;
                }
            }
            else if (selectType == SelectType.Horizontal)
            {
                if (selectedBlocls[0].GetCoordinate().y == block.GetCoordinate().y
                && Mathf.Abs(selectedBlocls[wordcount - 1].GetCoordinate().x - block.GetCoordinate().x) == 1)
                {
                    SelectCharacter(block);
                }
                else
                {
                    return;
                }
            }
            else
            {
                return;
            }
        }


    }


    public void SelectCharacter(Block block)
    {
        selectedBlocls.Add(block);
        HightLightBorder();
        UIController.Instance.UpdateCurrentWord(GetCurrentWord());
    }

    public void SwapBlock(Block block)
    {
        borderCurrentWord.ClearBorder();
        if(block.isNotMove == true)
        {
            selectedBlocls.Clear();
            return;
        }
        if (!currentPlayerBoard.ConstainBlock(block))
            return;
        if(selectedBlocls[0] != block)
        {
            currentPlayerBoard.SwapBlock(selectedBlocls[0], block);
        }
        selectedBlocls = new List<Block>();
    }

    public void UnSelectBlock()
    {
        if (selectedBlocls.Count == 0)
            return;
        if (selectedBlocls.Count == 1)
        {
            if (selectedBlocls[0].isNotMove == false)
            {
                InputController.Instance.inputType = InputType.Tap;
            }
            else
            {
                borderCurrentWord.ClearBorder();
                selectedBlocls.Clear();

            }
        }
        else
        {
            string word = GetCurrentWord();
            UIController.Instance.UpdateCurrentWord("");

            if (Dictionary.Instance.IsValidWord(word))
            {
                UIController.Instance.ShowNotice("Found " + word);
                if (selectType == SelectType.Horizontal)
                    currentPlayerBoard.AddWord(EWORD_TYPE.DEFENSE, new List<Block>(selectedBlocls));
                if (selectType == SelectType.Vertical)
                    currentPlayerBoard.AddWord(EWORD_TYPE.ATTACK, new List<Block>(selectedBlocls));
                if (selectedBlocls.Count == 0)
                {
                    Debug.LogError(word + " word is invalid");
                    //SwitchTurn();
                }
            }
            else
            {
                UIController.Instance.ShowNotice(word + " is not a word");
            }

            borderCurrentWord.ClearBorder();

            selectedBlocls.Clear();

        }



    }

    string GetCurrentWord()
    {
        string word = "";
        foreach (Block block in selectedBlocls)
        {
            word += block.GetCharacter();

        }
        return word;
    }

    public List<WordAction> listDestroyWords = new List<WordAction>();
    public List<WordAction> listWordAttacked = new List<WordAction>();
    public List<GameObject> effectattacks = new List<GameObject>();
    // public List<WordAction> listDestroyWords = new List<WordAction>();
    public void Attack(WordAction word)
    {
        //position
        int col = word.blocks[0].GetCoordinate().x;
        //check any defense
        //waitingPlayerBoard.wordActions();
        List<WordAction> wordDefenses = waitingPlayerBoard.GetWordDefenseAtCol(col);
        //attack && effect
        EffectController.Instance.PlayEffectAttack(currentPlayerBoard.GetworldPosition(word.blocks[0].GetCoordinate()));
        int strengthOfAttack = word.strength;
        while (wordDefenses.Count > 0 && strengthOfAttack > 0)
        {
            if (strengthOfAttack >= wordDefenses[0].strength)
            {
                strengthOfAttack -= wordDefenses[0].strength;
                //wordDefenses[0].destroy
                if (!listDestroyWords.Contains(wordDefenses[0]))
                    listDestroyWords.Add(wordDefenses[0]);
                wordDefenses[0].strength = 0;
                wordDefenses.RemoveAt(0);
            }
            else
            {
                //reduce strength
                wordDefenses[0].strength -= strengthOfAttack;
                wordDefenses[0].UpdateStrength();
                strengthOfAttack = 0;

            }

        }
        if (strengthOfAttack > 0)
            waitingPlayerBoard.ReduceHeal(strengthOfAttack);

        listWordAttacked.Add(word);
    }

    public void RemoveBlank()
    {
        CancelCurrentAction();
        if (phase == EPhase.ACTION)
            currentPlayerBoard.RemoveBlankCharacter();
    }

    public void CancelCurrentAction()
    {
        borderCurrentWord.ClearBorder();
        selectedBlocls.Clear();
        InputController.Instance.inputType = InputType.None;
    }

    int HighestAvaiable(int col)
    {
        for (int i = Gameconstain.GAME_ROW - 1; i >= 0; i--)
        {
            if (currentPlayerBoard._blocks[col, i] == null)
                return i;
        }
        return -1;
    }

    //rearrange block
    public void RearrangeBoard()
    {
        //lockinput
        //animation
        for (int i = 0; i < Gameconstain.GAME_ROW; i++)
        {
            int targetRow = 0;

            for (int j = Gameconstain.GAME_COL - 1; j >= 0; j--)
            {
                targetRow = HighestAvaiable(i);

                if (currentPlayerBoard._blocks[i, j] != null)
                {
                    if (j < targetRow && currentPlayerBoard._blocks[i, j].isNotMove == false)
                    {
                        Block block = currentPlayerBoard._blocks[i, j];
                        block.transform.DOKill(false);
                        block.transform.DOMove(currentPlayerBoard.GetworldPosition(i, targetRow), Gameconstain.TIME_ANIMATE_BLOCK * (targetRow - j));
                        block.SetCoordinate(new Coordinate(i, targetRow));
                        currentPlayerBoard._blocks[i, targetRow] = block;
                        currentPlayerBoard._blocks[i, j] = null;
                    }

                }
                else
                {

                }

            }

            //move from opponent's board to player's board
            targetRow = HighestAvaiable(i);
            int takePos = 0;
            while (targetRow >= 0)
            {
                //
                if (waitingPlayerBoard._blocks[i, takePos] != null && waitingPlayerBoard._blocks[i, takePos].isNotMove == false)
                {
                    if (currentPlayerBoard._blocks[i, targetRow] == null)
                    {
                        Block block = waitingPlayerBoard._blocks[i, takePos];
                        currentPlayerBoard._blocks[i, targetRow] = block;
                        waitingPlayerBoard._blocks[i, takePos] = null;
                        block.transform.DOKill(false);
                        block.transform.DOMove(currentPlayerBoard.GetworldPosition(i, targetRow), Gameconstain.TIME_ANIMATE_BLOCK * (takePos + targetRow));
                        block.SetCoordinate(new Coordinate(i, targetRow));
                    }
                    targetRow--;
                }
                takePos++;
            }


            //create new block
            //targetRow = HighestAvaiable(i);
            //int posCreateBlock = -1;
            //while (targetRow >= 0)
            //{

            //    GameObject newObj = BoardGameController.Instance.GetBlockFromPool();
            //    Block newBlock = newObj.GetComponent<Block>();
            //    newObj.SetActive(true);
            //    UpdateBlock(newBlock, i, targetRow);
            //    newBlock.SetCharacter(WordUtils.GetRandomCharacter());
            //    currentPlayerBoard._blocks[i, targetRow].transform.position = GetworldPosition(i, posCreateBlock);
            //    newBlock.transform.DOMove(GetworldPosition(i, targetRow), Gameconstain.TIME_ANIMATE_BLOCK * (targetRow - posCreateBlock));
            //    targetRow--;
            //    posCreateBlock--;
            //}
            waitingPlayerBoard.RearrangeBoard();
        }
    }
    //create new block




    //draw border
    public void HightLightBorder()
    {
        List<Block> blocks = selectedBlocls.OrderBy(x => x.GetCoordinate().x + x.GetCoordinate().y).ToList();
        int lastIndex = blocks.Count - 1;
        Vector3 firstBlockPosition = blocks[0].transform.position;
        Vector3 lastBlockPosition = blocks[lastIndex].transform.position;

        Vector3 center = (firstBlockPosition + lastBlockPosition) / 2;
        Vector3 size = new Vector3(Mathf.Abs(firstBlockPosition.x - lastBlockPosition.x) + blocks[0].blockSprite.bounds.size.x,
        Mathf.Abs(firstBlockPosition.y - lastBlockPosition.y) + blocks[0].blockSprite.bounds.size.y);
        borderCurrentWord.DrawBorder(center, size, blocks.Count>1?selectType:SelectType.None);
    }

    public void Restart()
    {
        //clear board
        playerBoard.ClearBoard();
        opponentBoard.ClearBoard();
      

        //clear ui
        //clear state game
        InputController.Instance.inputType = InputType.None;
        selectType = SelectType.None;
        SetPhase(EPhase.NONE);

        //start game again
        StartGame();
    }

}

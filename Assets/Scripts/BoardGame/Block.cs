﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Coordinate 
{
    public int x = -1;
    public int y = -1;

    public Coordinate()
    {
        x = -1;
        y = -1;
    }
    public Coordinate(int x,int y)
    {
        this.x = x;
        this.y = y;
    }
}



public class Block : MonoBehaviour {

    public TextMeshPro characterText;
    public TextMeshPro scoreText;
    public TextMeshPro totalAttackText;
    public TextMeshPro totalDefenseText;
    public SpriteRenderer blockSprite;

    public SpriteRenderer outerEffectSwap;
    public SpriteRenderer outerEffectAttack;
    public SpriteRenderer outerEffectDefense;
    string character;

    Coordinate coor = new Coordinate();

    bool isSelectAble = false;

    public bool isNotMove = false;

    public int score;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    //
    public void SetCharacter(Character c)
    {
        this.score = c.score;
        characterText.SetText(c.character);
        scoreText.SetText(score>0?score.ToString():"");
        character = c.character;
        isSelectAble = true;
    }

    public void SetCoordinate(Coordinate coor)
    {
        this.coor = coor;
    }

    public Coordinate GetCoordinate()
    {
        return this.coor;
    }

    public bool IsSelectAble()
    {
        return isSelectAble;
    }

    public string GetCharacter()
    {
        return character;
    }

    public bool IsEmpty()
    {
        return character == Character.NONE.character;
    }

    //public void SelectCharacter()
    //{
    //    //outerEffectSwap.gameObject.SetActive(true);
    //}

    //public void SelectAttackCharacter()
    //{
    //    //outerEffectAttack.gameObject.SetActive(true);
    //    //outerEffectSwap.gameObject.SetActive(false);
    //}

    //public void SelectDefenseCharacter()
    //{
    //    //outerEffectDefense.gameObject.SetActive(true);
    //    //outerEffectSwap.gameObject.SetActive(false);
    //}

    //public void UnSelectCharacter()
    //{
    //    //outerEffectSwap.gameObject.SetActive(false);
    //    //outerEffectAttack.gameObject.SetActive(false);
    //    //outerEffectDefense.gameObject.SetActive(false);
    //}

    public bool IsBlank()
    {
        return character == " " || character == "";
    }
}
